package com.example.cyberforencis.repository;

import com.example.cyberforencis.entity.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
}

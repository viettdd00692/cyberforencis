package com.example.cyberforencis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CyberforencisApplication {

    public static void main(String[] args) {
        SpringApplication.run(CyberforencisApplication.class, args);
    }

}
